//
//  RequestMovies.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 20/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import Alamofire

protocol RequestMovies:RequestMovieDb {
    func requestData(_ additionalParams:[String:String]?, cancelPrevious:Bool, endPoint:Endpoint, completion:@escaping (ResponseMovies?, Error?) -> ()) 
}

//  MARK: - extensions (default behaviour)

extension RequestMovies {
    
    func requestData(_ additionalParams:[String:String]?, cancelPrevious:Bool, endPoint:Endpoint, completion:@escaping (ResponseMovies?, Error?) -> ()) {
        
        let parameters = additionalParams != nil ? params.merged(with:additionalParams!) : params
        
        if cancelPrevious {
            cancelPreviousRequests()
        }
        
        Alamofire.SessionManager.default.request(self.requestUrl(endPoint: endPoint), parameters: parameters).validate().responseJSON { response in
            debugPrint(response)
            
            switch response.result {
            case .success:
                if let json = response.result.value as? [String:Any] {
                    completion(ResponseMovies(json), nil)
                }
            case .failure(let error):
                completion(nil, error)
            }
            
        }
    }

    fileprivate func cancelPreviousRequests() {
        Alamofire.SessionManager.default.session.getAllTasks() { tasks in
            
            // Cancel previous requests in the session
            tasks.forEach { $0.cancel() }
        }
    }


}
