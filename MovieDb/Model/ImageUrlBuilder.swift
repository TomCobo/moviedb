//
//  ImageUrlBuilder.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

class ImageUrlBuilder {
    
    var baseUrl:String?
    var size:String?
    var imagePath:String?
    
    func baseUrl(_ baseUrl:String?) -> ImageUrlBuilder {
        self.baseUrl = baseUrl
        return self
    }
    
    func size(_ size:String?) -> ImageUrlBuilder {
        self.size = size
        return self
    }
    
    func imagePath(_ imagePath:String?) -> ImageUrlBuilder {
        self.imagePath = imagePath
        return self
    }
    
    func build() -> String? {
        
        guard var imageUrl = baseUrl else {
            return nil
        }
        
        if size != nil { imageUrl += size! }
        
        if imagePath != nil { imageUrl += imagePath! }
        
        return imageUrl
    }
    
}
