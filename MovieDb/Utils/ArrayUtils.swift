//
//  ArrayUtils.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 20/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

class ArrayUtils {
    
    class func indexPathsFrom(_ start:Int, to end:Int) -> Array<IndexPath> {
        var arrayWithIndexPaths = Array<IndexPath>()
        for index in start..<end {
            arrayWithIndexPaths += [IndexPath(row: index, section: 0)]
        }
        return arrayWithIndexPaths
    }
    
}
