//
//  AppDelegate.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    //  MARK: - App lifecycle
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupWindow()
        
        return true
    }
    
    //  MARK: - Setup Window
    
    fileprivate func setupWindow() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = .white
        window.makeKeyAndVisible()
        window.rootViewController = getNavigationController()
        self.window = window
    }
    
    fileprivate func getNavigationController() -> UINavigationController {
        let navigationController = UINavigationController(rootViewController:HomeViewController())
        UINavigationBar.appearance().barTintColor = ThemeManager.shared.current.colorPrimaryDark
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: ThemeManager.shared.current.colorPrimary]
        return navigationController
    }

}

