//
//  HomeViewController.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

enum PageType:Int {
    case popular = 0, search
}

class HomeViewController: BaseViewController {
    
    fileprivate let homeRepository = MoviesRepository()
    fileprivate let configurationRepository = ImageConfigRepository() // TODO: change this to splash screen
    
    fileprivate var currentPage:PageType = .popular
    fileprivate var currentSearchText:String = ""
    
    override internal var rootView: HomeRootView {
        if _rootView == nil {
            _rootView = HomeRootView(self, pageType: .popular)
        }
        return _rootView as! HomeRootView
    }
    
    //  MARK: UICollectionView lifecycle
    
    override func loadView() {
        super.loadView()
        setupNavigationBar()
        loadConfiguration()
    }
    
    fileprivate func setupNavigationBar() {
        title = LocalizableStrings.appTitle.localized()
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: popularButton),
                                                   UIBarButtonItem(customView: searchButton)]
    }
    
    //  MARK: Navigation Bar Buttons
    
    fileprivate lazy var searchButton:UIButton = {
        let searchButton = ThemeManager.shared.current.searchButton
        searchButton.addTarget(self, action: #selector(buttonSearch(_:)), for: .touchUpInside)
        return searchButton
    }()
    
    fileprivate lazy var popularButton:UIButton = {
        let popularButton = ThemeManager.shared.current.popularButton
        popularButton.addTarget(self, action: #selector(buttonPopular(_:)), for: .touchUpInside)
        return popularButton
    }()
    
    fileprivate func loadFirstPage() {
        loadNextPage(1)
    }
    
    //  MARK: Load Data
    
    fileprivate func loadConfiguration() {
        configurationRepository.requestConfiguration() { response in
            self.loadFirstPage()
            
            if response == .OK {
                self.loadFirstPage()
            } else {
                self.rootView.showErrorLoadingData()
            }
        }
    }
    
    fileprivate func loadNextPage(_ nextIndex:Int) {
        homeRepository.requestPopularMovies(page: nextIndex) { response, error in
            self.rootView.data = response
        }
    }
    
    fileprivate func loadSearch(_ nextIndex:Int, with term:String) {
        homeRepository.requestSearchMovies(page: nextIndex, searchTerm: term) { response, error in
            self.rootView.data = response
        }
    }
    
}

//  MARK: Button Actions

extension HomeViewController {
    
    func buttonSearch(_ sender : UIButton){
        toogleViewMode(.search)
    }
    
    func buttonPopular(_ sender : UIButton){
        toogleViewMode(.popular)
    }
    
    fileprivate func toogleViewMode(_ newMode:PageType) {
        if currentPage != newMode {
            currentPage = newMode
            if newMode == .popular {
                loadFirstPage()
            }
            rootView.switchMode(newMode)
        }
    }
    
}

//  MARK: HomeRootViewDelegate

extension HomeViewController:HomeRootViewDelegate {
    
    func homeLoadNext(_ nextIndex:Int) {
        if currentPage == .popular {
            loadNextPage(nextIndex)
        } else {
            loadSearch(nextIndex, with: currentSearchText)
        }
    }
    
    func homeSearchWithTerm(_ searchTerm:String) {
        delay(delay: 0.7) {
            self.currentSearchText = searchTerm
            self.loadSearch(1, with:searchTerm)
        }
    }
    
}
