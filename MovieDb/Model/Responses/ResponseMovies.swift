//
//  ResponseMovies.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import SwiftyJSON

struct ResponseMovies:Response {
    
    let page:Int
    let results:Array<Movie>
    let totalResults: Int
    let totalPages: Int
    
    init(_ dict:[String:Any]) {
        let json = JSON(dict)
        page = json["page"].intValue
        totalResults = json["total_results"].intValue
        totalPages = json["total_pages"].intValue
        results = json["results"].arrayValue.map { Movie($0) }
    }
    
}
