//
//  HomeRootView.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography

protocol HomeRootViewDelegate:class {
    func homeLoadNext(_ nextPage:Int)
    func homeSearchWithTerm(_ searchTerm:String)
}

class HomeRootView: BaseView {
    
    fileprivate var delegate:HomeRootViewDelegate?
    fileprivate var searchHeightConstraint:NSLayoutConstraint!
    
    fileprivate var currentPage:Int! = 1
    fileprivate var currentPageType = PageType.popular
    
    
    init(_ delegate:HomeRootViewDelegate, pageType:PageType) {
        super.init(frame: .zero)
        self.delegate = delegate
        self.moviesDataSource.delegate = delegate
        customizeBackground()
        addTapGestureToDismissKeyboard()
    }
    
    fileprivate func customizeBackground() {
        backgroundColor = ThemeManager.shared.current.colorAccent
    }

    fileprivate func addTapGestureToDismissKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeRootView.dismissKeyboard))
        addGestureRecognizer(tap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK: - public method(s)
    
    func switchMode(_ newMode: PageType) {
        currentPage = 1
        currentPageType = newMode
    
        clearSearchTerm(newMode)
        animateSearchBar(newMode)
        scrollCollectionViewToTop()
        dismissKeyboard()
    }
    
    fileprivate func clearSearchTerm(_ newMode: PageType) {
        if newMode == .popular { searchBar.text = "" }
    }
    
    fileprivate func scrollCollectionViewToTop() {
        collectionView.setContentOffset(.zero, animated: true)
    }
    
    func dismissKeyboard() {
        searchBar.endEditing(true)
    }
    
    fileprivate func animateSearchBar(_ newMode: PageType) {
        searchHeightConstraint.constant = newMode == .search ? 40 : 0
        UIView.animate(withDuration: 0.6) {
            self.layoutIfNeeded()
        }
    }
    
    //  MARK: - setupSubvies
    
    override func setupSubviews() {
        addSubview(resultsMessage)
        addSubview(searchBar)
        addSubview(collectionView)
        addSubview(loadingIndicator)
    }
    
    override func setupAutolayout() {
        
        constrain(resultsMessage) { view in
            view.top == view.superview!.top + 130
            view.leading == view.superview!.leading + 20
            view.trailing == view.superview!.trailing - 20
        }
        
        constrain(searchBar) { view in
            view.top == view.superview!.top + 64
            view.leading == view.superview!.leading
            view.trailing == view.superview!.trailing
            self.searchHeightConstraint = view.height == (self.currentPageType == .search ? 40 : 0)
        }
        
        constrain(collectionView, searchBar) { view, view2 in
            view.top == view2.bottom
            view.bottom == view.superview!.bottom
            view.leading == view.superview!.leading + 20
            view.trailing == view.superview!.trailing - 20
        }
        
        constrain(loadingIndicator) { view in
            view.center == view.superview!.center
        }
        
    }
    
    //  MARK: - subviews
    
    fileprivate lazy var resultsMessage: UILabel = {
        return ThemeManager.shared.current.resultsMessage
    }()
    
    fileprivate lazy var searchBar: UISearchBar = {
        let searchBar = ThemeManager.shared.current.searchBar
        searchBar.delegate = self
        return searchBar
    }()
    
    fileprivate lazy var collectionView: UICollectionView = {
        let collectionView = ThemeManager.shared.current.homeCollectionView
        collectionView.dataSource = self.moviesDataSource
        collectionView.delegate = self.moviesDataSource
        return collectionView
    }()
    
    fileprivate lazy var loadingIndicator: UIActivityIndicatorView = {
        return ThemeManager.shared.current.loadingIndicator
    }()
    
    fileprivate lazy var moviesDataSource:MoviesDataSource = {
        return MoviesDataSource()
    }()
    
    //  MARK: - data
    
    var data:ResponseMovies? {
        didSet {
            if let movies = data?.results {
                currentPage = data!.page
                includeToDataSource(data!)
                updateUI(with: movies)
            } else if let dataSource = moviesDataSource.movies, dataSource.count  == 0 {
                showErrorLoadingData()
            }
        }
    }
    
    fileprivate func includeToDataSource(_ data:ResponseMovies?) {
        moviesDataSource.updateDataSource(data!)
    }
    
    fileprivate func updateUI(with movies:Array<Movie>) {
        
        showCollectionViewAnimated()
        showNoResultsIfNeeded()

        if (currentPage == 1 || movies.count == 0) {
            collectionView.reloadData()
            self.collectionView.invalidateIntrinsicContentSize()
        } else {
            refreshNewItems(with: movies)
        }
    }
    
    fileprivate func showNoResultsIfNeeded() {
        resultsMessage.alpha = (moviesDataSource.movies?.count ?? 0) == 0 ? 1.0 : 0.0
    }
    
    func showErrorLoadingData() {
        loadingIndicator.stopAnimating()
        resultsMessage.text = LocalizableStrings.errorLoadingData.localized()
        resultsMessage.alpha = 1.0
        collectionView.alpha = 0.0
    }
    
    fileprivate func refreshNewItems(with movies:Array<Movie>) {
        let resultBeforeUpdate = movies.count
        collectionView.performBatchUpdates({
            self.collectionView.insertItems(at: ArrayUtils.indexPathsFrom(resultBeforeUpdate,
                                                                          to: resultBeforeUpdate + movies.count))
            self.collectionView.invalidateIntrinsicContentSize()
        })
    }
    
    fileprivate func showCollectionViewAnimated() {
        if data?.page == 1 {
            loadingIndicator.stopAnimating()
            UIView.animate(withDuration: 0.6, animations: {
                self.collectionView.alpha = 1.0
            })
        }
    }

}

extension HomeRootView:UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count > 2 {
            collectionView.setContentOffset(.zero, animated: true)
            delegate?.homeSearchWithTerm(searchText)
        }
    }
    
}
