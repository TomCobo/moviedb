//
//  BaseView.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography

class BaseView:UIView {

    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupSubviews()
        setupAutolayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupAutolayout() { }
    
    func setupSubviews() { }
    
}
