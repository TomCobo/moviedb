//
//  Endpoints.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

enum Endpoint:String {
    case configuration = "configuration"
    case popular = "movie/popular"
    case search = "search/movie"
}
