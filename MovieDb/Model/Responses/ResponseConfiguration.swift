//
//  ImageResponse.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import SwiftyJSON


struct ResponseConfiguration {
    
    static func setupConfiguration(_ dict:[String:Any]) {
        let json = JSON(dict)
        ImageConfiguration.shared = ImageConfiguration(json["images"].dictionaryValue)
    }
    
}
