//
//  Respository.swift
//  MoviesApp
//
//  Created by T Cobo Martinez on 17/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import Alamofire

protocol RequestMovieDb {
    var baseUrl:String { get }
    var params:[String : String] { get }
    func requestUrl(endPoint:Endpoint) -> String
}

//  MARK: - extensions (default behaviour)

extension RequestMovieDb {
    var baseUrl:String { return Enviroment.sharedInstance.baseUrl.absoluteString }
    var params: [String : String] { return Enviroment.sharedInstance.defaultRequestParameters }
    func requestUrl(endPoint:Endpoint) -> String { return baseUrl + endPoint.rawValue  }
}

