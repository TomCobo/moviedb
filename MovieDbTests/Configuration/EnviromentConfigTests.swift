//
//  EnviromentConfigTests.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import XCTest

@testable import MovieDb
class EnviromentConfigTests: XCTestCase {
    
    var envDev:Enviroment? = nil
    
    override func setUp() {
        super.setUp()
        envDev = Enviroment.sharedInstance
    }
    
    func testEviromentIsNotEmpty() {
        XCTAssertNotNil(envDev, "Test - Enviroment: sharedInstance has not being created")
    }
    
    func testEviromentConfomsToConfigProtocol() {
        XCTAssert(envDev is Configuration, "Test - Enviroment: does not conforms to Config Protocol")
    }
    
    func testEviromentConfomsToUrlConfigProtocol() {
        XCTAssert(envDev is UrlConfig, "Test - Enviroment: does not conforms to UrlConfig Protocol")
    }
    
    func testEviromentDefaultConfigurationIsDev() {
        XCTAssert(envDev?.currentConfig == .dev, "Test - Enviroment: default: configuration is not Dev")
    }

    func testEviromentDevBaseUrlIsSetUp() {
        XCTAssertNotNil(envDev?.baseUrl, "Test - Enviroment: default: baseUrl is Empty")
        XCTAssertEqual(envDev?.baseUrl.absoluteString, "https://api.themoviedb.org/3/", "Eviroment default: baseUrl not set up properly")
    }
    
    func testEviromentDevParametersAreSetUp() {
        let paramenters = ["api_key": "93aea0c77bc168d8bbce3918cefefa45", "language": "en-US"]
        
        XCTAssertNotNil(envDev?.defaultRequestParameters, "Test - Enviroment: default: defaultRequestParameters is Empty")
        XCTAssertEqual(envDev!.defaultRequestParameters, paramenters, "Test - Enviroment: default: defaultRequestParameters not set up properly")
    }
    
    
}
