//
//  DiplayUtils.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

class DisplayUtils {
    
    static let screenHeight =  screenSize().height
    static let screenWidth =  screenSize().width
    
    class func screenSize() -> CGSize {
        let screenBounds: CGRect = UIScreen.main.bounds
        
        let screenSize = DeviceUtils.isDeviceOrientationLandscape() ?
            CGSize(width: screenBounds.size.height, height: screenBounds.size.width) :
            CGSize(width: screenBounds.size.width, height: screenBounds.size.height)
        
        return screenSize
    }
    
}
