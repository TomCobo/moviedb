//
//  HomeViewControllerTests.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 21/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//
import XCTest

@testable import MovieDb
class HomeViewControllerTests:XCTestCase  {
    
    
    var homeViewController:HomeViewController?
    var homeRootView:HomeRootView?
    
    override func setUp() {
        super.setUp()
        homeViewController = HomeViewController()
        homeViewController?.loadView()
    }
    
    func testHomeViewControllerConformsToHomeRootViewDelegate() {
        XCTAssert(homeViewController is HomeRootViewDelegate, "Test - HomeViewController: does not comform to HomeRootViewDelegate protocol")
    }
    
    func testHomeViewNavigationBar() {
        XCTAssertEqual(homeViewController?.title, LocalizableStrings.appTitle.localized(), "Test - HomeViewController: does not has the right title")
        XCTAssertNotNil(homeViewController?.navigationItem.rightBarButtonItem, "Test - HomeViewController: does not has buttons in the nav bar")
    }
    
    func testHomeViewRootView() {
        XCTAssertEqual(homeViewController?.rootView, homeViewController?.view, "Test - HomeViewController: RootView not assignates")
    }
    
    
    
}
