//
//  Model.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import SwiftyJSON

protocol MovieProtocol {
    var isAdult:Bool { get }
    var backdropPath:String { get }
    var genreIds:Array<Int> { get }
    var id:Int { get }
    var originalLanguage:String { get }
    var originalTitle:String { get }
    var overview:String { get }
    var popularity:Double { get }
    var posterPath:String { get }
    var releaseDate:String { get }
    var title:String { get }
    var voteAverage:Double { get }
    var voteCount:Int { get }
    var hasVideo:Bool { get }
}

// MARK: Inmutable Movie

struct Movie:MovieProtocol {
    
    let isAdult:Bool
    let backdropPath:String
    let genreIds:Array<Int>
    let id:Int
    let originalLanguage:String
    let originalTitle:String
    let overview:String
    let popularity:Double
    let posterPath:String
    let releaseDate:String
    let title:String
    let voteAverage:Double
    let voteCount:Int
    let hasVideo:Bool
    
    init(_ json:JSON) {
        isAdult = json["adult"].intValue == 1
        backdropPath = json["backdrop_path"].stringValue
        genreIds = json["genre_ids"].arrayValue.map{ $0.int! }
        id = json["id"].intValue
        originalLanguage = json["original_language"].stringValue
        originalTitle = json["original_title"].stringValue
        overview = json["overview"].stringValue
        popularity = json["popularity"].doubleValue
        posterPath = json["poster_path"].stringValue
        releaseDate = json["release_date"].stringValue
        title = json["title"].stringValue
        voteAverage = json["vote_average"].doubleValue
        voteCount = json["vote_count"].intValue
        hasVideo = json["video"].boolValue
    }
    
    func yearFormated() -> String {
        guard let year = DateUtils.getYearFromString(releaseDate) else {
            return LocalizableStrings.noDateAvailable.localized()
        }
        return "(\(year))"
    }
    
    func getTitleWithDate() -> String  {
        return (title + " " + yearFormated())
    }

}

// MARK: Equatable

extension Movie: Equatable {}

func ==(lhs:Movie, rhs:Movie) -> Bool {
    
    return lhs.isAdult == rhs.isAdult
    && lhs.backdropPath == rhs.backdropPath
    && lhs.genreIds == rhs.genreIds
    && lhs.id == rhs.id
    && lhs.originalLanguage == rhs.originalLanguage
    && lhs.originalTitle == rhs.originalTitle
    && lhs.overview == rhs.overview
    && lhs.popularity == rhs.popularity
    && lhs.posterPath == rhs.posterPath
    && lhs.releaseDate == rhs.releaseDate
    && lhs.title == rhs.title
    && lhs.voteAverage == rhs.voteAverage
    && lhs.voteCount == rhs.voteCount
    && lhs.hasVideo == rhs.hasVideo
}
