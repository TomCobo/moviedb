# Movie Db #

Repository which displays a list of popular movies and allow to search. This project is using CocoaPods, so in order to run the Target 'MovieDb' it has to be done opening the workspace.

### External Libraries (CocoaPods) ###

* Alamofire & AlamofireImage
* SwiftyJSON
* Cartography

### Project characteristics ###

* Swift
* Autolayout
* Has the UI built programatically
* MVC
* Has ThemeManager to modify UI quickly

### Unit Tests added ###

* Model 100% covered
* Includes an example of UIViewController test (as example)

### TODO (not included due to the lack of time) ###

* Add more tests for Utils and Extensions
* Add test for DataSource
* Add more tests for UI (UIViews, Themes,..)
* Include Mocking external library as OCMock
* Include a SplashScreen which would download the configurations instead of doing it on the home.
* Manage better offline mode