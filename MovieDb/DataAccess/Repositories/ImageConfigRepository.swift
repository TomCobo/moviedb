//
//  ImageRepository.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

class ImageConfigRepository:RequestConfiguration {
    
    public func requestConfiguration(p_ completion: @escaping (ResponseValue) -> ()) {
        requestData(endPoint: .configuration) { response in
            completion(response)
        }
    }
    
}
