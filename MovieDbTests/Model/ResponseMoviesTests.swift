//
//  ResponseMoviesTests.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//
import XCTest

@testable import MovieDb
class ResponseMoviesTests:XCTestCase  {
    
    func testResponseMovies() {
        FileManager.deserializeJSONFile("popular_response") { data, responseError in
            
            guard let dataResponse = data, responseError == nil else {
                XCTFail("Test - ResponseMovies: data from deserialization is empty or has error")
                return
            }
            
            checkValues(ResponseMovies(dataResponse))
        }
    }
    
    fileprivate func checkValues(_ responseMovies: ResponseMovies) {
        XCTAssertEqual(responseMovies.page, 1, "Test - ResponseMovies: error in page")
        XCTAssertEqual(responseMovies.totalResults, 19677, "Test - ResponseMovies: error in totalResults")
        XCTAssertEqual(responseMovies.totalPages, 984, "Test - ResponseMovies: error in totalPages")
        XCTAssertEqual(responseMovies.results.count, 2, "Test - ResponseMovies: error has not rigth number of results")
        XCTAssertEqual(responseMovies.results.first, MovieMock.firstItemMock(), "Test - ResponseMovies: error has not rigth number of results")
    }
    
}
