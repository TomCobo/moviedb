//
//  MovieCollectionViewCell.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography
import AlamofireImage

class MovieCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customizeBackground()
        setupSubviews()
        setupAutolayout()
    }
    
    fileprivate func customizeBackground() {
        backgroundColor = ThemeManager.shared.current.colorPrimaryDark
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK: - subviews
    
    fileprivate lazy var title:UILabel = {
        return ThemeManager.shared.current.homeCellTitle
    }()
    
    fileprivate lazy var overview:UILabel = {
        return ThemeManager.shared.current.homeCellOverview
    }()
    
    
    fileprivate lazy var picture:UIImageView = {
        return ThemeManager.shared.current.homeCellPicture
    }()
    
    //  MARK: - setup layout
    
    fileprivate func setupSubviews() {
        addSubview(picture)
        addSubview(title)
        addSubview(overview)
    }
    
    fileprivate func setupAutolayout() {
        
        constrain(picture) { view in
            view.leading == view.superview!.leading
            view.width == view.superview!.width
            view.height == 200
        }
        
        constrain(title, picture) { view, view2 in
            view.leading == view.superview!.leading + 20
            view.trailing == view.superview!.trailing - 20
            view.top == view2.bottom + 10
        }
        
        constrain(overview, title) { view, view2 in
            view.leading == view2.leading
            view.trailing == view2.trailing
            view.top == view2.bottom + 10
        }
        
    }
    
    //  MARK: - data
    
    var data:Movie? {
        didSet {
            if let movie = data {
                updateUI(with:movie)
            }
        }
    }
    
    fileprivate func updateUI(with movie:Movie) {
        
        title.text = movie.getTitleWithDate()
        overview.text = movie.overview
        
        if let imagePath = ImageUrlBuilder().baseUrl(ImageConfiguration.shared?.baseUrl).size(ImageConfiguration.shared?.backDropSizes[2]).imagePath(data?.backdropPath).build(),
            let imageUrl = URL(string: imagePath) {

            picture.af_setImage(withURL: imageUrl, placeholderImage: UIImage(named:"placeholder"), imageTransition: .crossDissolve(0.2))
            
        }
    
    }
    
    override func prepareForReuse() {
        title.text = nil
        overview.text = nil
        picture.image = nil
    }
    
}

