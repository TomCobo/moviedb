//
//  MoviesRepository.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

class MoviesRepository:RequestMovies {
    
    public func requestPopularMovies(page:Int, _ completion: @escaping (ResponseMovies?, Error?) -> ()) {
        
        let parameters = params.merged(with:["page":"\(page)"])

        requestData(parameters, cancelPrevious: false, endPoint: .popular) { response, error in
            completion(response, error)
        }
    }
    
    public func requestSearchMovies(page:Int, searchTerm:String , _ completion: @escaping (ResponseMovies?, Error?) -> ()) {
        
        let parameters = params.merged(with:["page":"\(page)", "query": searchTerm])
        
        requestData(parameters, cancelPrevious: true, endPoint: .search) { response, error in
            completion(response, error)
        }
    }

}
