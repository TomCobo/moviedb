//
//  RequestConfiguration.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 20/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import Alamofire

protocol RequestConfiguration:RequestMovieDb {
    func requestData(endPoint:Endpoint, completion:@escaping (ResponseValue) -> ())
}

//  MARK: - extensions (default behaviour)

extension RequestConfiguration {
    
    func requestData(endPoint:Endpoint, completion:@escaping (ResponseValue) -> ()) {
        
        Alamofire.request(requestUrl(endPoint: endPoint), parameters: params).responseJSON { response in
            debugPrint(response)
            switch response.result {
            case .success:
                if let json = response.result.value as? [String:Any]  {
                    ResponseConfiguration.setupConfiguration(json)
                    completion(.OK)
                } else {
                    completion(.Failed)
                }
            case .failure(_):
                completion(.Failed)
            }
            
        }
        
    }
}
