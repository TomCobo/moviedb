//
//  FileManager.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

typealias responseCompletion = ((_ data:[String:Any]?, _ error:FileManagerError?) -> Void)

enum FileManagerError: Error {
    case empty
    case noFile
    case serialize
    case unknown
}

extension FileManagerError: CustomStringConvertible {
    var description: String {
        let FileManagerError:String
        switch self {
        case .empty: FileManagerError = "no data on file found"
        case .noFile: FileManagerError = "no file found"
        case .serialize: FileManagerError = "serialization"
        case .unknown: FileManagerError = "unknown"
        }
        return "Error FileManager: " + FileManagerError
    }
}

class FileManager {
    
    class func deserializeJSONFile(_ fileName:String, responseData:responseCompletion) {
        
        var resultError:FileManagerError?
        var resultData:[String:Any]?
        
        defer {
            responseData(resultData, resultError)
        }
        
        do {
            guard let filePath = Bundle(for: type(of: FileManager())).path(forResource: fileName,ofType:"json") else {
                throw FileManagerError.noFile
            }
            
            guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath), options:NSData.ReadingOptions.uncached) else {
                throw FileManagerError.serialize
            }
            
            guard let results =  try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) else {
                throw FileManagerError.empty
            }
            
            resultData = results as? [String:Any]
        }
        catch let error as FileManagerError {
            resultError = error
        }
        catch {
            resultError = FileManagerError.unknown
        }
        
        
    }
    
}
