//
//  EnviromentConfig.swift
//  MoviesApp
//
//  Created by T Cobo Martinez on 17/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

enum Config {
    case dev
    case prod
}

protocol Configuration {
    var currentConfig:Config { get }
}

protocol UrlConfig {
    var baseUrl:URL { get }
    var defaultRequestParameters:[String : String] { get }
}

extension Configuration {
    var currentConfig:Config { return .dev }
}

struct Enviroment:UrlConfig, Configuration {
    
    static let sharedInstance = Enviroment()
    
    var baseUrl: URL {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https";
        urlComponents.host = host;
        urlComponents.path = "/\(apiVersion)/";
        return urlComponents.url!
    }
    
    var defaultRequestParameters: [String : String] { return ["api_key": apiKey,
                                                              "language": "en-US"] }
    
    fileprivate var host:String {
        switch(currentConfig) {
            case .dev: return "api.themoviedb.org"
            case .prod: return "prodHostUrl"
        }
    }
    
    fileprivate var apiKey:String {
        switch(currentConfig) {
            case .dev: return "93aea0c77bc168d8bbce3918cefefa45"
            case .prod: return "prodkey"
        }
    }
    
    fileprivate var apiVersion:String {
        switch(currentConfig) {
        case .dev: return "3"
        case .prod: return "2"
        }
    }
    
}

