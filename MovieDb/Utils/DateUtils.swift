//
//  DateUtils.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

class DateUtils {
    
    class func getYearFromString(_ dateString: String) -> Int? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)
        return date != nil ? Calendar.autoupdatingCurrent.dateComponents([.year], from: date!).year : nil
    }
    
}
