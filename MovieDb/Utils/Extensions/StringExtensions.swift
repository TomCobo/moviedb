//
//  StringExtensions.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

extension String {
    
    func height(constrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        
        return self.boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
                                            options: .usesLineFragmentOrigin,
                                            attributes: [NSFontAttributeName: font], context: nil).height
    }
    
    func localized(with comment:String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
    
}
