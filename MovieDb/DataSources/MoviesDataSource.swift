//
//  MoviesDataSource.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

class MoviesDataSource:NSObject, UICollectionViewDataSource {
    
    weak var delegate:HomeRootViewDelegate?
    
    var movies: [Movie]?
    fileprivate var currentPage:Int! = 1
    fileprivate var totalResults:Int! = 1
    fileprivate var totalPages:Int! = 1
    
    func updateDataSource(_ data: ResponseMovies) {
        self.currentPage = data.page
        self.totalResults = data.totalResults
        self.totalPages = data.totalPages
        addMovies(data.results)
    }
    
    fileprivate func addMovies(_ movies: [Movie]) {
        if self.movies == nil || currentPage == 1 {
            self.movies?.removeAll()
            self.movies = movies
        } else {
            self.movies!.append(contentsOf: movies)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:MovieCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MovieCollectionViewCell
        cell.data = movies?[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies?.count ?? 0
    }
    
}

extension MoviesDataSource: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let currentItemsCount = self.movies!.count
        if totalResults > indexPath.row && indexPath.row == currentItemsCount-6 && currentPage < totalPages {
            self.delegate?.homeLoadNext(currentPage + 1)
        }
    }
    
}

extension MoviesDataSource:UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellHeight:CGFloat = 0
        
        if let movie = self.movies?[indexPath.row] {
            cellHeight = 250
            cellHeight += movie.title.height(constrainedWidth: DisplayUtils.screenWidth - 80, font: ThemeManager.shared.current.fontPrimaryMedium)
            cellHeight += movie.overview.height(constrainedWidth: DisplayUtils.screenWidth - 80, font: ThemeManager.shared.current.fontPrimarySmall)
        }
        
        return CGSize(width: DisplayUtils.screenWidth - 40, height:cellHeight)
    }
    
}
