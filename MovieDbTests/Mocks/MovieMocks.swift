//
//  MovieMocks.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import Foundation
import SwiftyJSON

@testable import MovieDb
class MovieMock {
    
    class func firstItemMock() -> Movie {
        
        let jsonObj = JSON(["poster_path":"/45Y1G5FEgttPAwjTYic6czC9xCn.jpg",
                            "adult":false,
                            "overview":"In the near future",
                            "original_language":"en",
                            "title":"Logan",
                            "release_date":"2017-02-28",
                            "genre_ids":[ 28, 18, 878],
                            "id":263115,
                            "original_title":"Logan",
                            "backdrop_path":"/5pAGnkFYSsFJ99ZxDIYnhQbQFXs.jpg",
                            "popularity":147.22,
                            "vote_count":1357,
                            "video":false,
                            "vote_average":7.7])
        
        return Movie(jsonObj)
    }

}
