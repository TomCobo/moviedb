//
//  DeviceUtils.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

class DeviceUtils {
    
    class func isDeviceIPad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
    }
    
    class func isDeviceIPhone() -> Bool {
        return !DeviceUtils.isDeviceIPad()
    }
    
    class func isDeviceOrientationLandscape() -> Bool {
        return  UIApplication.shared.statusBarOrientation.isLandscape
    }
    
}
