//
//  MovieTests.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//
import XCTest

@testable import MovieDb

class MovieTests:XCTestCase {
    
    var movie = MovieMock.firstItemMock()
    
    func testMovieValuesAssigned() {
        XCTAssertEqual(movie.isAdult, false, "Test - Movie: value not assignated isAdult")
        XCTAssertEqual(movie.posterPath, "/45Y1G5FEgttPAwjTYic6czC9xCn.jpg", "Test - Movie: value not assignated posterPath")
        XCTAssertEqual(movie.overview, "In the near future", "Test - Movie: value not assignated overview")
        XCTAssertEqual(movie.originalLanguage, "en", "Test - Movie: value not assignated originalLanguage")
        XCTAssertEqual(movie.title, "Logan", "Test - Movie: value not assignated title")
        XCTAssertEqual(movie.releaseDate, "2017-02-28", "Test - Movie: value not assignated releaseDate")
        XCTAssertEqual(movie.genreIds, [ 28, 18, 878 ], "Test - Movie: value not assignated genreIds")
        XCTAssertEqual(movie.id, 263115, "Test - Movie: value not assignated id")
        XCTAssertEqual(movie.originalTitle, "Logan", "Test - Movie: value not assignated originalTitle")
        XCTAssertEqual(movie.backdropPath, "/5pAGnkFYSsFJ99ZxDIYnhQbQFXs.jpg", "Test - Movie: value not assignated backdropPath")
        XCTAssertEqual(movie.popularity, 147.22, "Test - Movie: value not assignated popularity")
        XCTAssertEqual(movie.voteCount, 1357, "Test - Movie: value not assignated voteCount")
        XCTAssertEqual(movie.hasVideo, false, "Test - Movie: value not assignated hasVideo")
        XCTAssertEqual(movie.voteAverage, 7.7, "Test - Movie: value not assignated voteAverage")
    }
    
    
}
