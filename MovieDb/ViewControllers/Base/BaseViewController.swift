//
//  BaseViewController.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    internal var _rootView: BaseView? = nil
    internal var rootView: BaseView? { return _rootView }

    override func loadView() {
        view = rootView
    }
    
}
