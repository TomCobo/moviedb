//
//  LocalizableItems.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 20/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

enum LocalizableStrings:String {
    
    case messageNoResuls = "message_no_results"
    case appTitle = "app_title"
    case noDateAvailable = "no_date_available"
    case errorLoadingData = "error_loading_data"
    
    func localized() -> String {
        return rawValue.localized()
    }
    
}
