//
//  Image.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//
import SwiftyJSON

protocol ImgConfiguration {
    var baseUrl:String { get }
    var backDropSizes:Array<String> { get }
}

struct ImageConfiguration:ImgConfiguration {
    
    static var shared:ImgConfiguration?
    
    let baseUrl:String
    let backDropSizes:Array<String>
    
    init(_ dict:[String:Any]) {
        let json = JSON(dict)
        baseUrl = json["secure_base_url"].stringValue
        backDropSizes = json["backdrop_sizes"].arrayValue.map { size in String(size.stringValue) }
    }
    
}
