//
//  ThemeManager.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 20/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

class ThemeManager {
    
    static let shared = ThemeManager()
    
    // Open to include new themes
    
    var current = MovieDbClearTheme()
    
}
