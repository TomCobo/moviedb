//
//  ThemeManager.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

struct MovieDbClearTheme:Theme {
    
    // MARK: Common colours
    
    var colorPrimary:UIColor { return .white }
    var colorPrimaryDark:UIColor { return UIColor(rgb:0x0A76BB) }
    var colorAccent:UIColor { return UIColor(rgb:0x2E3130) }
    
    //  MARK: Common fonts names
    
    var fontPrimaryName:String { return "Helvetica Neue" }
    
    //  MARK: Common fonts
    
    var fontPrimarySmall:UIFont { return UIFont(name: fontPrimaryName, size: 12)! }
    var fontPrimaryMedium:UIFont { return UIFont(name: fontPrimaryName, size: 14)! }
    var fontPrimaryBig:UIFont { return UIFont(name: fontPrimaryName, size: 16)! }
    
    
    //  MARK: Navigation Bar
    
    fileprivate var commonNavButton:UIButton {
        let popularButton = UIButton(type: .custom)
        popularButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        return popularButton
    }
    
    var popularButton:UIButton {
        let popularButton = commonNavButton
        popularButton.setImage(UIImage(named: "popular"), for: .normal)
        return popularButton
    }
    
    var searchButton:UIButton {
        let searchButton = commonNavButton
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        return searchButton
    }
    
    //  MARK: Home RootView
    
    var resultsMessage: UILabel {
        let resultsMessage = UILabel()
        resultsMessage.textColor = colorPrimary
        resultsMessage.textAlignment = .center
        resultsMessage.font = fontPrimaryBig
        resultsMessage.numberOfLines = 0
        resultsMessage.alpha = 0.0
        resultsMessage.text = LocalizableStrings.messageNoResuls.localized()
        return resultsMessage
    }
    
    var searchBar: UISearchBar {
        let searchBar = UISearchBar(frame:.zero)
        searchBar.barTintColor = colorPrimaryDark
        return searchBar
    }
    
    var homeCollectionView: UICollectionView {
        let collectionView = UICollectionView(frame:.zero, collectionViewLayout: self.collectionViewLayout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.alpha = 0.0
        return collectionView
    }
    
    fileprivate var collectionViewLayout: UICollectionViewFlowLayout {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        collectionViewLayout.minimumInteritemSpacing = 0
        collectionViewLayout.minimumLineSpacing = 20
        collectionViewLayout.scrollDirection = UICollectionViewScrollDirection.vertical
        return collectionViewLayout
    }
    
    var loadingIndicator: UIActivityIndicatorView {
        let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        loadingIndicator.tintColor = colorPrimary
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()
        return loadingIndicator
    }
    
    //  MARK: HomeCell
    
    var homeCellTitle:UILabel {
        let title = UILabel()
        title.textColor = colorPrimary
        title.font = fontPrimaryMedium
        title.numberOfLines = 0
        return title
    }
    
    var homeCellOverview:UILabel {
        let overview = UILabel()
        overview.textColor = colorPrimary
        overview.font = fontPrimarySmall
        overview.numberOfLines = 0
        return overview
    }
    
    
    var homeCellPicture:UIImageView {
        let image = UIImageView()
        image.contentMode = .center
        image.clipsToBounds = true
        image.backgroundColor = colorAccent
        return image
    }
    

    
}
