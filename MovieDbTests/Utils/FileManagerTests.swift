//
//  FileManagerTests.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 18/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import XCTest

class FileManagerTests: XCTestCase {

    func testFileNotFound() {
        deserialiseData("this_file_does_not_exits") { data, error in
            XCTAssertNil(data, "Test - FileManager: data should be empty")
            XCTAssertEqual(error, FileManagerError.noFile, "Error: error should be no file")
        };
    }
    
    func testFileDeserialationFailed() {
        deserialiseData("popular_failed") { data, error in
            XCTAssertNil(data, "Test - FileManager: data should be empty")
            XCTAssertEqual(error, FileManagerError.empty, "Test - Error: error should be empty")
        };
    }
    
    func testFileDeserialationSuccess() {
        deserialiseData("popular_response") { data, error in
            XCTAssertNotNil(data, "Test - FileManager: data should has data")
            XCTAssertNil(error, "Test - FileManager: error should be empty")
        };
    }
    
    fileprivate func deserialiseData(_ fileName:String, responseData:((_ data:[String:Any]?, _ error:FileManagerError?) -> Void)) {
        FileManager.deserializeJSONFile(fileName) {
            responseData($0.0, $0.1)
        }
    }
    
}
