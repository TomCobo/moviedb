//
//  Theme.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

protocol Theme {
    
    // MARK: - Common colours
    
    var colorPrimary:UIColor { get }
    var colorPrimaryDark:UIColor { get }
    var colorAccent:UIColor { get }
    
    //  MARK: - Common fonts names
    
    var fontPrimaryName:String { get }
    
    //  MARK: Common fonts
    
    var fontPrimarySmall:UIFont { get }
    var fontPrimaryMedium:UIFont { get }
    var fontPrimaryBig:UIFont { get }
    
    //  MARK: Navigation Bar
    
    var popularButton:UIButton { get }
    var searchButton:UIButton { get }
    
    //  MARK: Home RootView
    
    var resultsMessage: UILabel { get }
    var searchBar: UISearchBar  { get }
    var homeCollectionView: UICollectionView  { get }
    var loadingIndicator: UIActivityIndicatorView { get }
    
    //  MARK: HomeCell
    
    var homeCellTitle:UILabel { get }
    var homeCellOverview:UILabel { get }
    var homeCellPicture:UIImageView { get }

    
}
