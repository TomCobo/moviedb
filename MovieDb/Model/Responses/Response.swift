//
//  Response.swift
//  MovieDb
//
//  Created by T Cobo Martinez on 19/03/2017.
//  Copyright © 2017 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

protocol Response {
    var page:Int { get }
    var totalResults:Int { get }
    var totalPages:Int { get }
    var results:Array<Movie> { get }
}

enum ResponseValue {
    case OK, Failed
}
